package testCases.registeredUser;

import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.UserUpdatePage;
import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.*;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class RegisteredUserProfileUpdateTest {

    static UserUpdatePage userProfilePage;


    @BeforeClass
    public static void beforeClass(){
        LogInPage logInPage = new LogInPage();
        logInPage.loginUser("2");

        userProfilePage = new UserUpdatePage("userProfile.url2");
    }

    @Before
    public void beforeTest(){
        userProfilePage.navigateToUserProfilePage();
    }

    @Test
    public void a_RegisteredUser_Should_BeAbleToUpdateName(){
        userProfilePage.updateName("userUpdatePage.updateFirstName", "userUpdatePage.updateLastName");
        userProfilePage.saveButton();

        userProfilePage.assertFirstNameIsChanged();
    }

    @Test
    public void b_RegisteredUser_Should_BeAbleToUpdateAge(){
        userProfilePage.updateAge("userUpdatePage.updateAge");
        userProfilePage.saveButton();

        userProfilePage.assertAgeIsChanged();
    }

    @Test
    public void c_RegisteredUser_Should_BeAbleToUpdateDescription(){
        userProfilePage.updateDescription("userUpdatePage.updateDescription");
        userProfilePage.saveButton();

        userProfilePage.assertDescriptionIsChanged();
    }

    @Test
    public void d_RegisteredUser_Should_BeAbleToUpdateVisibility(){
        userProfilePage.updateVisibility("userUpdatePage.visibilityPrivateOption");
        userProfilePage.saveButton();

        userProfilePage.assertVisibilityIsChangedToConnections();
    }

    @Test
    public void e_RegisteredUser_Should_BeAbleToUpdateGender(){
        userProfilePage.updateGender("userUpdatePage.genderFemaleOption");
        userProfilePage.saveButton();

        userProfilePage.assertGenderIsChangedToFemale();
    }

    @AfterClass
    public static void end(){
        LogInPage logInPage = new LogInPage();

        userProfilePage.navigateToUserProfilePage();

        userProfilePage.resetProfile("userUpdatePage.restFirstName2", "userUpdatePage.resetLastName2",
                "userUpdatePage.resetAge", "userUpdatePage.resetDescription",
                "userUpdatePage.visibilityPublicOption", "userUpdatePage.genderMaleOption",
                "userUpdatePage.resetNationalityBulgarian");

        logInPage.logout();

        UserActions.quitDriver();
    }
}