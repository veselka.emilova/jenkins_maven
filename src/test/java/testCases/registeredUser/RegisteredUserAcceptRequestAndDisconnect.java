package testCases.registeredUser;

import com.telerikacademy.finalproject.API.API;
import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.RequestsPage;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import testCases.BaseTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegisteredUserAcceptRequestAndDisconnect extends BaseTest {

    @Before
    public void sendRequest_login() {
// User ID for https://fast-taiga-64065.herokuapp.com/ is 28
// User ID for https://infinite-basin-89429.herokuapp.com/ is 2

        API.sendConnectRequest("2","3", actions.getDriver());
    }

    @Test
    public void acceptRequest_Disconnect(){
        LogInPage logInPage = new LogInPage();
        logInPage.loginUser("2");

        RequestsPage requestsPage = new RequestsPage();
        requestsPage.acceptFriendRequest();

        requestsPage.assertUsersConnected();

        requestsPage.disconnect();

        requestsPage.assertUsersNotConnected();

        logInPage.logout();}
}