package testCases.registeredUser;

import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.UsersPage;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import testCases.BaseTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class RegisteredUserUsersTests extends BaseTest {

    UsersPage usersPage = new UsersPage();;

    @BeforeClass
    public static void startUp(){
        LogInPage logInPage = new LogInPage();
        logInPage.loginUser("2");
    }

    @Test
    public void a_RegistersUser_Should_BeAbleToSearchOtherUsers(){
        usersPage.searchUser();

        usersPage.assertUserFound();
    }

    @Test
    public void b_RegistersUser_Should_BeAbleToOpenOtherUsersProfile(){
        usersPage.selectUser();

        usersPage.assertUserPageOpened();
    }

    @Test
    public void c_RegistersUser_Should_BeAbleToSendConnectRequest(){
        usersPage.sendConnectRequest();

        usersPage.assertConnectRequestSent();
    }

    @Test
    public void d_RegistersUser_Should_BeAbleToRejectSentConnectRequest(){
        usersPage.rejectSentConnectRequest();

        usersPage.assertSentConnectRequestRejected();
    }
}
