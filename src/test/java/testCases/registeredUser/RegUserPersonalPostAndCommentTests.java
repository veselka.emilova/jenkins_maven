package testCases.registeredUser;

import com.telerikacademy.finalproject.pages.*;
import org.junit.*;
import org.junit.runners.MethodSorters;
import testCases.BaseTest;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegUserPersonalPostAndCommentTests extends BaseTest {

    @BeforeClass
    public static void RegUser_Login() {
        LogInPage logInPage = new LogInPage();
        logInPage.loginUser("1");

        logInPage.assertLoginSuccessful();
    }

    //This has to be removed after "RegUser unable to delete post" bug is fixed
    @AfterClass
    public static void Admin_Login_Delete_RegUserPost() {
        LogInPage logInPage = new LogInPage();
        logInPage.loginUser("3");

        LatestPostsPage latestPostsPage = new LatestPostsPage();
        latestPostsPage.deleteFirstPost();
    }

    @Test
    public void a_RegUser_Should_BeAbleToCreatePost() {
        NewPostPage newPostPage = new NewPostPage();
        newPostPage.newPostCreate("P", "N", "Described");

        newPostPage.assertPostCreated();
    }

//    EXPECTING BUG TO BI FIXED
//    @Test
//    public void b_RegUser_Should_BeAbleToEditCreatedPost() {
//        ProfilePage profilePage = new ProfilePage("profile1.url");
//
//        postPage.editPost();
//        postPage.assertPostEdited();
//    }

    @Test
    public void c_RegUser_Should_BeAbleToCommentCreatedPost() {
        ProfilePage profilePage = new ProfilePage("profile1.url");
        profilePage.navigateToPage();

        PostPage postPage = new PostPage();
        postPage.commentPost();

        postPage.assertCommentCreated();

    }

    @Test
    public void d_RegUser_Should_BeAbleToLikeAndDislikeComment() {
        ProfilePage profilePage = new ProfilePage("profile1.url");
        profilePage.navigateToPage();

        PostPage postPage = new PostPage();
        postPage.likeComment();
        postPage.assertCommentLiked();

        postPage.dislikeComment();
        postPage.assertCommentDisliked();
    }

//    EXPECTING BUG TO BI FIXED
//    @Test
//    public void e_RegUser_Should_BeAbleToEditComment() {
//        ProfilePage profilePage = new ProfilePage("profile1.url");
//          profilePage.navigateToPage();
//
//        PostPage postPage = new PostPage();
//        postPage.editComment();
//        postPage.assertCommentEdited();
//    }

//    EXPECTING BUG TO BI FIXED
//    @Test
//    public void f_RegUser_Should_BeAbleToDeleteComment() {
//        ProfilePage profilePage = new ProfilePage("profile1.url");
//        profilePage.navigateToPage();
//
//        PostPage postPage = new PostPage();
//        postPage.deleteComment();
//        postPage.assertCommentDeleted();
//    }

    @Test
    public void g_RegUser_Should_BeAbleToLikeDislikePost() {
        ProfilePage profilePage = new ProfilePage("profile1.url");
        profilePage.navigateToPage();

        PostPage postPage = new PostPage();
        postPage.likePost();
        postPage.assertPostLiked();

        postPage.dislikePost();
        postPage.assertPostDisliked();
    }

//    EXPECTING BUG TO BI FIXED
//    @Test
//    public void h_RegUser_Should_BeAbleToDeleteCreatedPost() {
//        ProfilePage profilePage = new ProfilePage("profile3.url");
//        profilePage.navigateToPage();
//
//    PostPage postPage = new PostPage();
//        postPage.deletePost();
//        postPage.assertPostDeleted();
//    }

}
