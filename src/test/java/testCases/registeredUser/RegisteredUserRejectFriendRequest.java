package testCases.registeredUser;

import com.telerikacademy.finalproject.API.API;
import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.RequestsPage;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import testCases.BaseTest;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class RegisteredUserRejectFriendRequest extends BaseTest {

    @Before
        public void sendRequest_login() {

        API.sendConnectRequest("2","3", actions.getDriver());
    }

    @Test
    public void rejectRequest() {
        LogInPage logInPage = new LogInPage();
        logInPage.loginUser("2");

        RequestsPage requestsPage = new RequestsPage();

        requestsPage.rejectFriendRequest();
        requestsPage.assertUsersNotConnected();

        logInPage.logout();
    }

}
