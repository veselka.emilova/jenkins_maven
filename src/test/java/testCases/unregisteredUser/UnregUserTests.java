package testCases.unregisteredUser;

import com.telerikacademy.finalproject.pages.LatestPostsPage;
import com.telerikacademy.finalproject.pages.UsersPage;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import testCases.BaseTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class UnregUserTests extends BaseTest {

    UsersPage usersPage = new UsersPage();

    @Test
    public void a_Unregistered_User_Should_BeAbleToSeeAndSearchUsers(){
        usersPage.searchUser();
        usersPage.assertUserFound();
    }

    @Test
    public void b_Unregistered_User_Should_BeAbleToOpenUserProfile(){
        usersPage.selectUser();
        usersPage.assertUserFound();
    }

    @Test
    public void c_Unregistered_User_Should_BeAbleToSeeAndOpenLatestPost(){
        LatestPostsPage latestPostsPage = new LatestPostsPage();
        latestPostsPage.selectLatestPost();
        latestPostsPage.assertPostOpened();
    }
}
