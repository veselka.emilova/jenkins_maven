package testCases;

import com.telerikacademy.finalproject.API.API;
import com.telerikacademy.finalproject.pages.NavigationPage;
import com.telerikacademy.finalproject.utils.Utils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class API_tests extends BaseTest{
    @Test
    public void navigateToHome_UsingNavigation(){
        API.authenticateDriverForUser("healthyFoodAdmin.username.encoded", "healthyFoodAdmin.pass.encoded", actions.getDriver());

        NavigationPage navPage = new NavigationPage();
        actions.clickElement(navPage.homeButton);
        navPage.assertPageNavigated();
        actions.assertElementPresent("mainPage.logout");
    }

    @Test
    public void a_createNationality(){
        API.createNatRequest(Utils.getUIMappingByKey("nationalities.nationalityName"), actions.getDriver());
    }

    @Test
    public void b_deleteNationality(){
        API.deleteNatRequest(actions.getDriver());
    }

    @Test
    public void getNationalities(){
        API.getNatRequest(actions.getDriver());
    }
}
