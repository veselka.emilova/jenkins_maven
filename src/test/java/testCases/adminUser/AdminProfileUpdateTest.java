package testCases.adminUser;

import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.UserUpdatePage;
import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.*;
import org.junit.runners.MethodSorters;
import testCases.BaseTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class AdminProfileUpdateTest {

    static UserUpdatePage adminProfilePage;

    @BeforeClass
    public static void beforeClass(){
        LogInPage logInPage = new LogInPage();
        logInPage.loginUser("3");

        adminProfilePage = new UserUpdatePage("userProfile.url3");
    }

    @Before
    public void startUp(){
        adminProfilePage.navigateToUserProfilePage();
    }

    @Test
    public void a_Admin_Should_BeAbleToUpdateName(){
        adminProfilePage.updateName("userUpdatePage.updateFirstName", "userUpdatePage.updateLastName");
        adminProfilePage.saveButton();

        adminProfilePage.assertFirstNameIsChanged();
    }

    @Test
    public void b_Admin_Should_BeAbleToUpdateAge(){
        adminProfilePage.updateAge("userUpdatePage.updateAge");
        adminProfilePage.saveButton();

        adminProfilePage.assertAgeIsChanged();
    }

    @Test
    public void c_Admin_Should_BeAbleToUpdateDescription(){
        adminProfilePage.updateDescription("userUpdatePage.updateDescription");
        adminProfilePage.saveButton();

        adminProfilePage.assertDescriptionIsChanged();
    }

    @Test
    public void d_Admin_Should_BeAbleToUpdateVisibility(){
        adminProfilePage.updateVisibility("userUpdatePage.visibilityPrivateOption");
        adminProfilePage.saveButton();

        adminProfilePage.assertVisibilityIsChangedToConnections();
    }

    @Test
    public void e_Admin_Should_BeAbleToUpdateGender(){
        adminProfilePage.updateGender("userUpdatePage.genderFemaleOption");
        adminProfilePage.saveButton();

        adminProfilePage.assertGenderIsChangedToFemale();
    }

    @Test
    public void f_Admin_Should_BeAbleToUpdateNationality(){
        adminProfilePage.updateNationality("userUpdatePage.updateNationalityAmerican");
        adminProfilePage.saveButton();

        adminProfilePage.assertNationalityIsChanged();
    }

    @AfterClass
    public static void end(){
        LogInPage logInPage = new LogInPage();

        adminProfilePage.navigateToUserProfilePage();

        adminProfilePage.resetProfile("userUpdatePage.restFirstName3", "userUpdatePage.resetLastName3",
                "userUpdatePage.resetAge", "userUpdatePage.resetDescription3",
                "userUpdatePage.visibilityPublicOption", "userUpdatePage.genderMaleOption",
                "userUpdatePage.resetNationalityBulgarian");

        logInPage.logout();

        UserActions.quitDriver();
    }
}