package testCases.adminUser;

import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.NationalitiesPage;
import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.*;
import org.junit.runners.MethodSorters;
import testCases.BaseTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class AdminNationalityTest extends BaseTest{

    static NationalitiesPage nationalityPage;

    @BeforeClass
    public static void startUp(){
        LogInPage logInPage = new LogInPage();
        logInPage.loginUser("3");
    }

    @Before
    public void beforeTest(){
        nationalityPage = new NationalitiesPage();
        nationalityPage.navigateToPage();
    }

    @Test
    public void a_CreateNationality(){
        nationalityPage.createNationality();

        nationalityPage.assertNationalityIsCreated();
    }

    @Test
    public void b_UpdateNationality(){
        nationalityPage.editNationality();

        nationalityPage.assertNationalityIsUpdated();
    }

    @Test
    public void c_DeleteNationality(){
        nationalityPage.deleteNationality();

        nationalityPage.assertNationalityIsDeleted();
    }

    @AfterClass
    public static void endClass(){
        LogInPage logInPage = new LogInPage();
        logInPage.logout();

        UserActions.quitDriver();
    }
}