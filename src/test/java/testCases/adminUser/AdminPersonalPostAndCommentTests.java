package testCases.adminUser;

import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.NewPostPage;
import com.telerikacademy.finalproject.pages.PostPage;
import com.telerikacademy.finalproject.pages.ProfilePage;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import testCases.BaseTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class AdminPersonalPostAndCommentTests extends BaseTest {

    ProfilePage profilePage = new ProfilePage("profile3.url");

     @BeforeClass
     public static void Admin_Should_BeAbleToLogin() {
         LogInPage logInPage = new LogInPage();
         logInPage.loginUser("3");
     }

     @Test
     public void a_Admin_Should_BeAbleToCreatePost() {
         NewPostPage newPostPage = new NewPostPage();
         newPostPage.newPostCreate("P", "N", "Described");

         newPostPage.assertPostCreated();
     }

     @Test
     public void b_Admin_Should_BeAbleToEditCreatedPost() {
         profilePage.navigateToPage();

         PostPage postPage = new PostPage();
         postPage.editPost();

         postPage.assertPostEdited();
     }

     @Test
     public void c_Admin_Should_BeAbleToCommentCreatedPost() {
         profilePage.navigateToPage();

         PostPage postPage = new PostPage();
         postPage.commentPost();

         postPage.assertCommentCreated();
     }

    @Test
    public void d_Admin_Should_BeAbleToLikeDislikeComment() {
        profilePage.navigateToPage();

        PostPage postPage = new PostPage();
        postPage.likeComment();
        postPage.assertCommentLiked();

        postPage.dislikeComment();
        postPage.assertCommentDisliked();
    }

     @Test
     public void e_Admin_Should_BeAbleToEditComment() {
         profilePage.navigateToPage();

         PostPage postPage = new PostPage();
         postPage.editComment();

         postPage.assertCommentEdited();
     }

     @Test
     public void f_Admin_Should_BeAbleToDeleteComment() {
         profilePage.navigateToPage();

         PostPage postPage = new PostPage();
         postPage.deleteComment();

         postPage.assertCommentDeleted();
     }

     @Test
     public void g_Admin_Should_BeAbleToLikeDislikePost() {
         profilePage.navigateToPage();

         PostPage postPage = new PostPage();
         postPage.likePost();
         postPage.assertPostLiked();

         postPage.dislikePost();
         postPage.assertPostDisliked();
     }

     @Test
     public void h_Admin_Should_BeAbleToDeleteCreatedPost() {
         profilePage.navigateToPage();

         PostPage postPage = new PostPage();
         postPage.deletePost();
         postPage.assertPostDeleted();
     }
}
