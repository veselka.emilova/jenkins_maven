package stepdefinitions;

import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.NewPostPage;
import com.telerikacademy.finalproject.utils.UserActions;
import org.jbehave.core.annotations.AfterStories;
import org.jbehave.core.annotations.BeforeStories;

public class BaseStepDefinitions {
    @BeforeStories
    public void beforeStories(){
        UserActions.loadBrowser("base.url");
        LogInPage logInPage = new LogInPage();
        logInPage.loginUser("3");
    }

    @AfterStories
    public void afterStories(){
        UserActions.quitDriver();
    }
}
