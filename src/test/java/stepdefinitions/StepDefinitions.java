package stepdefinitions;

import com.telerikacademy.finalproject.utils.UserActions;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class StepDefinitions extends BaseStepDefinitions{
    UserActions actions = new UserActions();

    @Given("Click $element element")
    @When("Click $element element")
    @Then("Click $element element")
    public void clickElement(String element){
        actions.isElementPresentUntilTimeout(element,10);
        actions.clickElement(element);
    }

    @Given("Type $value in $name field")
    @When("Type $value in $name field")
    @Then("Type $value in $name field")
    public void typeInField(String value, String field){
        actions.isElementPresentUntilTimeout(field,10);
        actions.typeValueInField(value, field);
    }

    @Given("Hover $element element")
    @When("Hover $element element")
    @Then("Hover $element element")
    public void hoverElement(String element){
        actions.isElementPresentUntilTimeout(element,10);
        actions.hoverElement(element);
        actions.waitFor(2000);
    }
}
