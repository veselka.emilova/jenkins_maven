Meta:
@endToEnd

Narrative:
As an admin
I want to create a post,
Edit the post,
Like my post,
Comment my post,
Edit my comment,
Like my comment,
Delete my comment,
Delete my post.


Scenario: End to end scenario in social network
!-- Create post
Given Hover mainPage.LatestPosts element
When Click mainPage.newPost element
Then Type P in newPostPage.visibility field
Then Type N in newPostPage.category field
Then Type BDDToBeDeleted in newPostPage.title field
Then Type "Created in Story mode" in newPostPage.description field
Then Click newPostPage.save element

!-- Edit post
When Click postPage.assertTitleBDD element
Then Click postPage.edit element
Then Type Edited in postPage.title field
Then Click postPage.save element

!-- Like post
When Click postPage.likeButton element

!-- Comment post
When Type BDDComment to be delete in postPage.comment field
Then Click postPage.sendComment element

!-- Edit comment
When Click postPage.editComment element
Then Type Edited in postPage.comment field
Then Click postPage.sendComment element

!-- Like comment
When Click postPage.likeCommentButton element

!-- Delete comment
When Click postPage.editComment element
Then Click postPage.delete element
Then Click postPage.deleteConfirm element

!-- Delete post
When Click postPage.edit element
Then Click postPage.delete element
Then Click postPage.deleteConfirm element

