package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;

import java.time.Instant;
import java.util.concurrent.TimeUnit;

public class NationalitiesPage extends BasePage {

    public NationalitiesPage() {
        super("admin.nationalities");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    // ***** Methods *****
    Instant instant = Instant.now(); // Current date-time in UTC.
    String output = instant.toString();
    public String nationality=Utils.getUIMappingByKey("nationalities.nationalityName") + output;

    public void createNationality() {
        actions.clickElementAfterWait("nationalities.addButton");

        actions.isElementPresentUntilTimeout("nationalities.nameField", 10);
        actions.typeValueInField(nationality, "nationalities.nameField");

        actions.clickElementAfterWait("nationalities.saveButton");
    }

    public void editNationality(){
        actions.clickElementAfterWait("nationalities.lastRowNationalityEditButton");

        actions.isElementPresentUntilTimeout("nationalities.nameField", 10);
        actions.typeValueInField(Utils.getUIMappingByKey("nationalities.nationalityUpdateName"), "nationalities.nameField");

        actions.clickElementAfterWait("nationalities.saveButton");
    }

    public void deleteNationality(){
        actions.clickElementAfterWait("nationalities.lastRowNationalityEditButton");

        actions.clickElementAfterWait("nationalities.deleteButton");

        actions.clickElementAfterWait("nationalities.confirmDeletionButton");
    }

    // ***** Asserts *****
    public void assertNationalityIsCreated(){
        actions.assertElementPresentAfterWait("nationalities.assertNationalityIsCreated");
    }

    public void assertNationalityIsUpdated(){
        actions.assertElementPresentAfterWait("nationalities.assertNationalityIsUpdated");
    }

    public void assertNationalityIsDeleted(){
        actions.isElementPresentUntilTimeout("nationalities.lastRowNationalityEditButton", 10);
        actions.assertElementIsNotPresent("nationalities.lastRowNationalityEditButton", "nationalities.nationalityUpdateName");
    }
}
