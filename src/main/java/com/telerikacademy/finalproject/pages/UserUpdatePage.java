package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;

import java.util.concurrent.TimeUnit;

public class UserUpdatePage extends BasePage{

    public UserUpdatePage(String userKeys) {
        super(userKeys);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    public void navigateToUserProfilePage(){
        navigateToPage();
    }


    public void updateName(String firstName, String lastName){
        actions.isElementPresentUntilTimeout("userUpdatePage.firstName", 10);
        actions.clearField("userUpdatePage.firstName");
        actions.typeValueInField(Utils.getUIMappingByKey(firstName), "userUpdatePage.firstName");

        actions.isElementPresentUntilTimeout("userUpdatePage.lastName", 10);
        actions.clearField("userUpdatePage.lastName");
        actions.typeValueInField(Utils.getUIMappingByKey(lastName), "userUpdatePage.lastName");
    }

    public void updateAge(String age){
        actions.isElementPresentUntilTimeout("userUpdatePage.age", 10);
        actions.clearField("userUpdatePage.age");
        actions.typeValueInField(Utils.getUIMappingByKey(age), "userUpdatePage.age");
    }

    public void updateDescription(String description){
        actions.isElementPresentUntilTimeout("userUpdatePage.description", 10);
        actions.clearField("userUpdatePage.description");
        actions.typeValueInField(Utils.getUIMappingByKey(description), "userUpdatePage.description");
    }

    public void updateVisibility(String visibility){
        actions.clickElementAfterWait("userUpdatePage.visibility");
        actions.clickElement(visibility);
    }

    public void updateGender(String gender){
        actions.clickElementAfterWait("userUpdatePage.gender");
        actions.clickElement(gender);
    }

    public void updateNationality(String nationality){
        actions.clickElementAfterWait("userUpdatePage.nationality");
        actions.clickElement(nationality);
    }

    public void saveButton(){
        actions.clickElementAfterWait("userUpdatePage.saveButton");
    }

    public void resetProfile(String firstName, String lastName, String age, String description, String visibility,
                             String gender, String nationality){
        updateName(firstName, lastName);
        updateAge(age);
        updateDescription(description);
        updateVisibility(visibility);
        updateGender(gender);
        updateNationality(nationality);
        saveButton();
    }

    // ***** Asserts *****
    public void assertFirstNameIsChanged() {
        actions.assertElementPresentAfterWait("profilePage.updatedUserName");
    }

    public void assertAgeIsChanged() {
        actions.assertElementPresentAfterWait("profilePage.updatedAge");
    }

    public void assertDescriptionIsChanged(){
        actions.assertElementPresentAfterWait("profilePage.updatedDescription");
    }

    public void assertVisibilityIsChangedToConnections(){
        actions.clickElementAfterWait("profilePage.EditButton");

        actions.isElementPresentUntilTimeout("userUpdatePage.visibilityPrivateOptionSelected", 10);
        actions.assertElementPresent("userUpdatePage.visibilityPrivateOptionSelected");
    }

    public void assertVisibilityIsChangedToPublic(){
        actions.clickElementAfterWait("profilePage.EditButton");

        actions.isElementPresentUntilTimeout("userUpdatePage.visibility", 10);
        actions.assertElementPresent("userUpdatePage.visibilityPublicOptionSelected");
    }

    public void assertGenderIsChangedToFemale(){
        actions.assertElementPresentAfterWait("profilePage.updatedGenderFemale");
    }

    public void assertGenderIsChangedToMale(){
        actions.assertElementPresentAfterWait("profilePage.updatedGenderMale");
    }

    public void assertNationalityIsChanged(){
        actions.assertElementPresentAfterWait("profilePage.updatedNationality");
    }
}
