package com.telerikacademy.finalproject.pages;

import java.util.concurrent.TimeUnit;

public class LatestPostsPage extends BasePage{

    public LatestPostsPage(){
        super("latestPosts.url");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void selectLatestPost(){
        navigateToPage();

        actions.clickElementAfterWait("latestPosts.firstPost");
    }

    public void assertPostOpened(){
        actions.assertElementPresentAfterWait("latestPosts.postOpened");
    }

    public void deleteFirstPost() {
        navigateToPage();

        actions.isElementPresentUntilTimeout("latestPosts.firstPost", 10);
        actions.clickElement("latestPosts.firstPost");

        actions.isElementPresentUntilTimeout("postPage.edit", 10);
        actions.clickElement("postPage.edit");

        actions.isElementPresentUntilTimeout("postPage.delete", 10);
        actions.clickElement("postPage.delete");

        actions.isElementPresentUntilTimeout("postPage.deleteConfirm", 10);
        actions.clickElement("postPage.deleteConfirm");
    }
}
