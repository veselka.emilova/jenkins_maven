package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;
import io.restassured.http.Cookies;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import com.telerikacademy.finalproject.utils.RequestHandler;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class LogInPage extends BasePage {
    public static Cookie ck;
    public static List restAssuredCookies;

    public LogInPage() {
        super("login.url");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void loginUser(String userKey) {
        String username = "";
        String password = "";

        switch (userKey) {
            case "1":
                username = Utils.getConfigPropertyByKey("users.username1");
                password = Utils.getConfigPropertyByKey("users.password1");
                break;
            case "2":
                username = Utils.getConfigPropertyByKey("users.username2");
                password = Utils.getConfigPropertyByKey("users.password2");
                break;
            case "3":
                username = Utils.getConfigPropertyByKey("users.username3");
                password = Utils.getConfigPropertyByKey("users.password3");
                break;
        }

        navigateToPage();

        actions.isElementPresentUntilTimeout("loginPage.username", 10);
        actions.typeValueInField(username, "loginPage.username");

        actions.isElementPresentUntilTimeout("loginPage.password", 10);
        actions.typeValueInField(password, "loginPage.password");

        actions.clickElement("loginPage.signIn");
    }

    public void assertLoginSuccessful(){
        actions.assertElementPresent("mainPage.logout");
    }

    public void logout(){
        actions.clickElementAfterWait("mainPage.logout");
    }
}
