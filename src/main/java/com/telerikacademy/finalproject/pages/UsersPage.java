package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;

import java.util.concurrent.TimeUnit;

public class UsersPage extends BasePage {
    String assertResult = Utils.getUIMappingByKey("usersPage.searchAssert") + Utils.getUIMappingByKey("usersPage.searchUser") + "')]";

    public UsersPage() {
        super("users.url");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }


    public void searchUser() {
        navigateToPage();

        String user = Utils.getUIMappingByKey("usersPage.searchUser");

        actions.isElementPresentUntilTimeout("usersPage.searchInput", 10);
        actions.typeValueInField(user, "usersPage.searchInput");

        actions.waitFor(1000);

        actions.clickElementAfterWait("usersPage.searchButton");
    }

    public void assertUserFound() {
        actions.waitFor(2000);
        actions.assertElementPresent(assertResult);
    }

    public void selectUser() {
        actions.clickElementAfterWait(assertResult);
    }

    public void assertUserPageOpened() {
        actions.waitFor(2000);
        actions.assertElementPresent(assertResult);
    }

    public void sendConnectRequest() {
        actions.clickElementAfterWait("usersPage.connectRequest");
    }

    public void assertConnectRequestSent() {
        actions.waitFor(2000);
        actions.assertElementPresent("usersPage.sentRequestReject");
    }

    public void rejectSentConnectRequest() {
        actions.clickElementAfterWait("usersPage.sentRequestReject");
    }

    public void assertSentConnectRequestRejected() {
        actions.waitFor(2000);
        actions.assertElementPresent("usersPage.connectRequest");
    }

}
