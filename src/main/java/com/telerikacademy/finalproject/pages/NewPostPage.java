package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;

import java.util.concurrent.TimeUnit;

public class NewPostPage extends BasePage {
    String toBeDeleted = Utils.getUIMappingByKey("newPostPage.FixedTitle");
    String assertTitle = Utils.getUIMappingByKey("newPostPage.assertTitle") + Utils.getUIMappingByKey("newPostPage.FixedTitle") + "')]";

    public NewPostPage() {
        super("newPost.url");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void newPostCreate(String visibility, String category, String description) {
        //visibility, category - only the first letter must be entered
        navigateToPage();

        String pictureLink = Utils.getUIMappingByKey("newPostPage.pictureLink");

//        actions.isElementPresentUntilTimeout("newPostPage.picture", 10);
//        actions.typeValueInField(pictureLink, "newPostPage.picture");

        actions.isElementPresentUntilTimeout("newPostPage.visibility", 10);
        actions.typeValueInField(visibility, "newPostPage.visibility");

        actions.isElementPresentUntilTimeout("newPostPage.category", 10);
        actions.typeValueInField(category, "newPostPage.category");

        actions.isElementPresentUntilTimeout("newPostPage.title", 10);
        actions.typeValueInField(toBeDeleted, "newPostPage.title");

        actions.isElementPresentUntilTimeout("newPostPage.description", 10);
        actions.typeValueInField(description, "newPostPage.description");

        actions.clickElementAfterWait("newPostPage.save");
    }

    public void assertPostCreated() {
        actions.assertElementPresentAfterWait(assertTitle);
    }




}
