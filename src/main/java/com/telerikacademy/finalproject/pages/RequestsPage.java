package com.telerikacademy.finalproject.pages;

import java.util.concurrent.TimeUnit;

public class RequestsPage extends BasePage{

    public RequestsPage(){
        super("restRequests.url");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void acceptFriendRequest(){
        navigateToPage();

        actions.clickElementAfterWait("requests.firstRequest");

        actions.clickElementAfterWait("requests.confirm");
    }

    public void disconnect(){
        actions.clickElementAfterWait("requests.disconnect");
    }

    public void rejectFriendRequest(){
        navigateToPage();

        actions.clickElementAfterWait("requests.firstRequest");

        actions.clickElementAfterWait("requests.reject");

        actions.waitFor(3000);
    }

    public void assertUsersConnected(){
        actions.assertElementPresent("usersPage.disconnect");
    }

    public void assertUsersNotConnected(){
        actions.assertElementPresent("usersPage.connectRequest");
    }

}
