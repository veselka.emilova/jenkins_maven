package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;

import java.util.concurrent.TimeUnit;

public class ProfilePage  extends BasePage{

    public ProfilePage(String userKey){
        super(userKey);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    public void navigateToProfilePage(){
        navigateToPage();
    }
}
