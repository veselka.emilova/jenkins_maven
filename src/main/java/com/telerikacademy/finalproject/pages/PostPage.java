package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;

public class PostPage extends BasePage{
    String postToBeDeletedEdited = Utils.getUIMappingByKey("postPage.FixedTitleEdited");
    String commentToBeDeleted = Utils.getUIMappingByKey("postPage.FixedComment");
    String commentToBeDeletedEdited = Utils.getUIMappingByKey("postPage.FixedEditedComment");
    String assertTitle = Utils.getUIMappingByKey("postPage.assertTitle") + Utils.getUIMappingByKey("newPostPage.FixedTitle") + "')]";
    String assertTitleEdited = Utils.getUIMappingByKey("postPage.assertTitleEdited") + Utils.getUIMappingByKey("newPostPage.FixedTitle") + Utils.getUIMappingByKey("postPage.FixedTitleEdited")+ "')]";
    String createdComment = Utils.getUIMappingByKey("postPage.createdComment") + Utils.getUIMappingByKey("postPage.FixedComment") + "')]";
    String editedComment = Utils.getUIMappingByKey("postPage.editedComment") + Utils.getUIMappingByKey("postPage.FixedEditedComment") + "')]";

    public PostPage() {
        super();
    }

    public void editPost() {
        actions.clickElementAfterWait(assertTitle);;

        actions.clickElementAfterWait("postPage.edit");

        actions.isElementPresentUntilTimeout("postPage.title", 10);
        actions.typeValueInField(postToBeDeletedEdited, "postPage.title");

        actions.clickElementAfterWait("postPage.save");
    }

    public void assertPostEdited() {
        actions.assertElementPresentAfterWait(assertTitleEdited);
    }

    public void likePost() {
        actions.clickElementAfterWait(assertTitle);

        actions.clickElementAfterWait("postPage.likeButton");
    }

    public void assertPostLiked() {
        actions.assertElementPresentAfterWait("postPage.oneLike");
    }

    public void dislikePost() {
        actions.clickElementAfterWait("postPage.dislikeButton");
    }

    public void assertPostDisliked() {
        actions.clickElementAfterWait("postPage.zeroLike");
    }

    public void commentPost() {
        actions.clickElementAfterWait(assertTitle);

        actions.isElementPresentUntilTimeout("postPage.comment", 10);
        actions.typeValueInField(commentToBeDeleted, "postPage.comment");

        actions.clickElementAfterWait("postPage.sendComment");
    }

    public void assertCommentCreated() {
        actions.assertElementPresentAfterWait(createdComment);
    }

    public void editComment() {
        actions.clickElementAfterWait(assertTitle);

        actions.clickElementAfterWait("postPage.editComment");

        actions.isElementPresentUntilTimeout("postPage.comment", 10);
        actions.typeValueInField(commentToBeDeletedEdited, "postPage.comment");

        actions.clickElementAfterWait("postPage.sendComment");
    }

    public void assertCommentEdited() {
        actions.assertElementPresentAfterWait(editedComment);
    }

    public void likeComment() {
        actions.clickElementAfterWait(assertTitle);

        actions.clickElementAfterWait("postPage.likeCommentButton");
    }

    public void assertCommentLiked() {
        actions.assertElementPresentAfterWait("postPage.oneCommentLike");
    }

    public void dislikeComment() {
        actions.clickElementAfterWait("postPage.dislikeCommentButton");
    }

    public void assertCommentDisliked() {
        actions.isElementPresentUntilTimeout("postPage.zeroCommentLike", 10);
        actions.assertElementPresent("postPage.zeroLike");
    }

    public void deleteComment() {
        actions.clickElementAfterWait(assertTitle);

        actions.clickElementAfterWait("postPage.editComment");

        actions.clickElementAfterWait("postPage.delete");

        actions.clickElementAfterWait("postPage.deleteConfirm");
    }

    public void assertCommentDeleted() {
        actions.assertElementPresentAfterWait("postPage.deletedComment");
    }

    public void deletePost() {
        actions.clickElementAfterWait(assertTitle);

        actions.clickElementAfterWait("postPage.edit");

        actions.clickElementAfterWait("postPage.delete");

        actions.clickElementAfterWait("postPage.deleteConfirm");
    }

//    НЯМАМ РЕШЕНИЕ ЗА ТОВА ВСЕ ОЩЕ
    public void assertPostDeleted() {
        actions.assertElementNotPresent(assertTitle);
    }

}
