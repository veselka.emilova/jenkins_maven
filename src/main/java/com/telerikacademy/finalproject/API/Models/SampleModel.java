package com.telerikacademy.finalproject.API.Models;

public class SampleModel {
    public float age;
    public String description;
    public String email;
    public boolean enabled;
    public String firstName;
    public float id;
    public String lastName;
}
