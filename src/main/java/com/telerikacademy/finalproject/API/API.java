package com.telerikacademy.finalproject.API;

import com.telerikacademy.finalproject.utils.PropertiesManager;
import com.telerikacademy.finalproject.utils.RequestHandler;
import com.telerikacademy.finalproject.utils.UserActions;
import com.telerikacademy.finalproject.utils.Utils;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;

import java.time.Instant;

public class API {
    public static int idNationality=0;

    public static void authenticateDriverForUser(String usernameKey, String passwordKey, WebDriver driver){
        String username = PropertiesManager.PropertiesManagerEnum.INSTANCE.getConfigProperties().getProperty(usernameKey);
        String password = PropertiesManager.PropertiesManagerEnum.INSTANCE.getConfigProperties().getProperty(passwordKey);

        //Resetting the Cookie, otherwise gives: org.openqa.selenium.InvalidCookieDomainException: Document is cookie-averse
        driver.get(Utils.getConfigPropertyByKey("base.url"));
        driver.manage().addCookie(new Cookie("JSESSIONID", "emptyCookie", Utils.getConfigPropertyByKey("domain"), "/", null, true, true));

        // Authenticate and extract cookie
        RequestHandler client = new RequestHandler();
        String requestBody = "username=" + username + "&password=" + password;
        Response response = client.sendPostRequest(Utils.getConfigPropertyByKey("authenticate.url"), requestBody, ContentType.URLENC);
        String cookieValue = response.getDetailedCookie("JSESSIONID").getValue();
        driver.manage().addCookie(new Cookie("JSESSIONID", cookieValue,  Utils.getConfigPropertyByKey("domain"), "/", null, true, true));
    }

    public static void logout(){
        RequestHandler client = new RequestHandler();

        String logoutUrl = Utils.getConfigPropertyByKey("base.url")+"logout";
        Response response = client.sendGetRequest(logoutUrl);
    }

    public void createPostRequest(int categoriesKey, int categoryKey, String descriptionKey, String titleKey,  int visibilityKey,  WebDriver driver){
        UserActions actions = new UserActions();

        authenticateDriverForUser("healthyFoodAdmin.username.encoded", "healthyFoodAdmin.pass.encoded", actions.getDriver());
        org.openqa.selenium.Cookie cookie = driver.manage().getCookieNamed("JSESSIONID");

//        String visibilityId = PropertiesManager.PropertiesManagerEnum.INSTANCE.getConfigProperties().getProperty(visibilityKey);
//        String title = PropertiesManager.PropertiesManagerEnum.INSTANCE.getConfigProperties().getProperty(titleKey);
//        String category = PropertiesManager.PropertiesManagerEnum.INSTANCE.getConfigProperties().getProperty(categoryKey);
//        String categories = PropertiesManager.PropertiesManagerEnum.INSTANCE.getConfigProperties().getProperty(categoriesKey);
//        String description = PropertiesManager.PropertiesManagerEnum.INSTANCE.getConfigProperties().getProperty(descriptionKey);

        RequestHandler client = new RequestHandler();

        JSONObject requestParams = new JSONObject();
        requestParams.put("visibilityId", visibilityKey);
        requestParams.put("description", descriptionKey);
//        requestParams.put("file", "(binary)");
        requestParams.put("title", titleKey);
        requestParams.put("_categories", categoryKey);

//        requestParams.put("categories", categories);

        String requestBody = requestParams.toJSONString();
        System.out.println(requestBody);
        Response response = client.sendPostRequest(Utils.getConfigPropertyByKey("restPostCreation.url"), requestBody, ContentType.JSON, cookie);

        int statusCode = response.getStatusCode();
        Assert.assertEquals(200, statusCode);
        String successCode = response.jsonPath().get("SuccessCode");
        Assert.assertEquals( "Correct Success code was returned", successCode, "OPERATION_SUCCESS");
    }

    public static void createNatRequest(String nationality, WebDriver driver){
        UserActions actions = new UserActions();

        authenticateDriverForUser("healthyFoodAdmin.username.encoded", "healthyFoodAdmin.pass.encoded", actions.getDriver());
        Cookie cookie = driver.manage().getCookieNamed("JSESSIONID");

        RequestHandler client = new RequestHandler();

        Instant instant = Instant.now(); // Current date-time in UTC.
        String output = instant.toString();
        System.out.println(output);
        nationality+=output;
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("nationality", nationality);
        System.out.println(jsonObj.toJSONString());
        String body = jsonObj.toJSONString();

        Response response = client.sendPostRequest(Utils.getConfigPropertyByKey("restNationalityCreation.url"), body, ContentType.JSON, cookie);

        response.getBody().print().contains("id");
        idNationality = response.jsonPath().getInt("id");
        System.out.println(idNationality);

        int statusCode = response.getStatusCode();
        Assert.assertEquals("Status Code is not 200!", 200, statusCode);
    }

    public static void deleteNatRequest(WebDriver driver){
        UserActions actions = new UserActions();

        authenticateDriverForUser("healthyFoodAdmin.username.encoded", "healthyFoodAdmin.pass.encoded", actions.getDriver());
        Cookie cookie = driver.manage().getCookieNamed("JSESSIONID");

        RequestHandler client = new RequestHandler();

//    No idea how to get the int id from createNatRequest();
        String deleteEndpoint = Utils.getConfigPropertyByKey("restNationalities.url") + idNationality;
        System.out.println(deleteEndpoint);
        Response response = client.sendDeleteRequest(deleteEndpoint, cookie);

//      int statusCode = response.getStatusCode();
//      Assert.assertEquals("Status Code is not 200!", 200, statusCode);
    }

    public static void getNatRequest(WebDriver driver){
        UserActions actions = new UserActions();

        authenticateDriverForUser("healthyFoodAdmin.username.encoded", "healthyFoodAdmin.pass.encoded", actions.getDriver());
        Cookie cookie = driver.manage().getCookieNamed("JSESSIONID");

        RequestHandler client = new RequestHandler();

        Response response = client.sendGetRequest(Utils.getConfigPropertyByKey("restNationalities.url"));

        int statusCode = response.getStatusCode();
        Assert.assertEquals("Status Code is not 200!", 200, statusCode);
    }

    public static void sendConnectRequest(String userId, String sender, WebDriver driver){
        String username = "";
        String password = "";

        switch (sender) {
            case "1":
                username="healthyFoodTestUser1.username.encoded";
                password="healthyFoodTestUser1.pass.encoded";
                break;
            case "2":
                username="healthyFoodTestUser2.username.encoded";
                password="healthyFoodTestUser2.pass.encoded";
                break;
            case "3":
                username="healthyFoodAdmin.username.encoded";
                password="healthyFoodAdmin.pass.encoded";
                break;
        }

        UserActions actions = new UserActions();

        authenticateDriverForUser(username, password, actions.getDriver());
        Cookie cookie = driver.manage().getCookieNamed("JSESSIONID");

        RequestHandler client = new RequestHandler();

        String urlBase = Utils.getConfigPropertyByKey("restUserCreation.url");
        String endpointURL = urlBase+userId;
        System.out.println(endpointURL);
        Response response = client.sendPostRequestNoBody(endpointURL, cookie);

        int statusCode = response.getStatusCode();
        Assert.assertEquals(200, statusCode);
    }
}
