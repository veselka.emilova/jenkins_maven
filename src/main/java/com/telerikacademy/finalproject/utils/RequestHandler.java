package com.telerikacademy.finalproject.utils;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Cookies;
import io.restassured.response.Response;
import org.openqa.selenium.Cookie;

import java.util.HashMap;
import java.util.Map;

public class RequestHandler {
    public Response sendGetRequest(String endpoint, HashMap<String, String> queryParams){
        return RestAssured.given()
                .contentType(ContentType.JSON)
                .queryParams(queryParams)
                .when()
                .get(endpoint)
                .then()
                .statusCode(200)
                .extract().response();
    }

    public Response sendGetRequest(String endpoint){
        return RestAssured.given()
                .contentType(ContentType.JSON)
                .when()
                .get(endpoint)
                .then()
                .statusCode(200)
                .extract().response();
    }

    public Response sendPostRequest(String endpoint, Object body, ContentType contentType){
        return RestAssured.given()
                .contentType(contentType)
                .body(body)
                .when()
                .post(endpoint)
                .then()
                .extract().response();
    }

    public Response sendPostRequest(String endpoint, Object body, ContentType contentType, Cookie cookie){
        System.out.println(cookie.getName());
        System.out.println(cookie.getValue());
        return RestAssured.given()
                .cookie(cookie.getName(), cookie.getValue())
                .contentType(contentType)
                .body(body)
                .when()
                .post(endpoint)
                .then()
                .extract().response();
    }

    public Response sendPostRequestNoBody(String endpoint, Cookie cookie){
        System.out.println(cookie.getName());
        System.out.println(cookie.getValue());
        return RestAssured.given()
                .cookie(cookie.getName(), cookie.getValue())
                .when()
                .post(endpoint)
                .then()
                .extract().response();
    }

    public Response sendDeleteRequest(String endpoint, Cookie cookie){
        return RestAssured.given()
                .contentType(ContentType.JSON)
                .cookie(cookie.getName(), cookie.getValue())
                .when()
                .delete(endpoint)
                .then()
                .assertThat()
                .statusCode(200)
                .extract().response();
    }
}